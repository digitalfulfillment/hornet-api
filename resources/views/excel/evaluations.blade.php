<table>
    <thead>
        <tr>
            <th>Employee Name</th>
            <th>Employee Number</th>
            <th>Campaign</th>
            <th>Team Leader</th>
            <th>Work Queue</th>
            <th>Log Date and Time</th>
            <th>Reference ID</th>
            <th>QA Name</th>
            <th>Evaluation Date</th>
            <th>Quality Score</th>
            <th>Auto Failed Field</th>
            <th>Interaction Information</th>
            @foreach ($questions as $question)
                <th>Q{{$loop->index + 1}}</th>
                <th>Q{{$loop->index + 1}} - Remarks</th>
            @endforeach
            <th>Coaching Remarks</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($evaluations as $evaluation)
        <tr>
            <td>{{$evaluation->agent->name}}</td>
            <td>{{$evaluation->agent->employee_number}}</td>
            <td>{{$evaluation->campaign->name}}</td>
            <td>{{$evaluation->team->name}}</td>
            <td>{{$evaluation->work_queue->name}}</td>
            <td>{{$evaluation->call_history}}</td>
            <td>{{$evaluation->reference_code}}</td>
            <td>{{$evaluation->user->name}}</td>
            <td>{{\Carbon\Carbon::parse($evaluation->created_at)->toFormattedDateString()}}</td>
            <td>{{$evaluation->score}}%</td>
            <td>{{$evaluation->auto_fail ? 'Yes' : 'No'}}</td>
            <td>
                @foreach($evaluation->call_details as $callDetail)
                    <span>{{$callDetail}}, </span>
                @endforeach
            </td>
            @foreach ($evaluation->questions as $question)
                <td>{{$question['rating']}}</td>
                <td>{{$question['remarks']}}</td>
            @endforeach
            <td>{{$evaluation->remarks}}</td>
        </tr>
        @endforeach
    </tbody>
</table>