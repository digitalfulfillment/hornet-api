<table>
    <thead>
        <tr>
            <th>Label</th>
            <th>Description</th>
            <th>Point Allocation</th>
        </tr>
    </thead>
    <tbody>
        @foreach($questions as $question)
            <tr>
                <td>Q{{$loop->index + 1}}</td>
                <td>{{$question['question']}}</td>
                <td>{{$question['points']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>