<table>
    <thead>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <th align="center">Quality Score</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Employee Name</th>
            <td align="center">{{ $evaluation->agent->name }}</td>
            <td align="center"></td>
            <td align="center">{{ $evaluation->score }}%</td>
        </tr>
        <tr>
            <th>Employee ID</th>
            <td align="center">{{ $evaluation->agent->employee_number }}</td>
        </tr>
        <tr>
            <th>Team Leader</th>
            <td align="center">{{ $evaluation->team->name }}</td>
        </tr>
        <tr>
            <th>Work Queue</th>
            <td align="center">{{ $evaluation->work_queue->name }}</td>
        </tr>
        <tr>
            <th>Date and Time</th>
            <td align="center">{{ \Carbon\Carbon::parse($evaluation->call_history)->toDateTimeString() }}</td>
        </tr>
        <tr>
            <th>Reference ID</th>
            <td align="center">{{ $evaluation->reference_code }}</td>
        </tr>
        <tr>
            <th>Evaluated by</th>
            <td align="center">{{ $evaluation->reference_code }}</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th>Interaction Information</th>
            <td></td>
        </tr>
    </thead>
    <tbody>
        @foreach($evaluation->call_details as $callDetail)
        <tr>
            <td align="center">{{ $callDetail['label'] }}</td>
            <td align="center">{{ $callDetail['value'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@foreach($evaluation->content as $category)
<table>
    <thead>
        <tr>
            <th>{{ $category['name'] }}</th>
            <th align="center">Rating</th>
            <th align="center">Point Allocation</th>
            <th align="center">Comments</th>
        </tr>
    </thead>
    <tbody>
        @foreach($category['questions'] as $question)
            <tr>
                <td>{{ $question['question'] }}</td>
                <td align="center">
                    @if( $question['applicable'] )
                        <span>{{ $question['score'] ? 'Yes' : 'No' }}</span>
                    @else
                        <span>N/A</span>
                    @endif
                </td>
                <td align="center">{{ $question['points'] }}</td>
                <td>{{ $question['remarks'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endforeach

<table>
    <thead>
        <tr>
            <th>Coaching Comments</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $evaluation->remarks }}</td>
        </tr>
    </tbody>
</table>