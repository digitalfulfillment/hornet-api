<?php

use Illuminate\Database\Seeder;

use App\Agent;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function(){
            Agent::create([
                'employee_number' => '10072005',
                'name' => 'Tosco, Germie'
            ]);
            
            Agent::create([
                'employee_number' => '10072012',
                'name' => 'Solano, Janella'
            ]);

            Agent::create([
                'employee_number' => '10072027',
                'name' => 'Gueco, Sheena'
            ]);

            Agent::create([
                'employee_number' => '10072022',
                'name' => 'Marvin, Sheena'
            ]);
            
            Agent::create([
                'employee_number' => '10072040',
                'name' => 'Fernandez, Raymond'
            ]);

            Agent::create([
                'employee_number' => '10072028',
                'name' => 'Vidiz, Tisha'
            ]);

            Agent::create([
                'employee_number' => '10072029',
                'name' => 'Sepillo, Alexis Ericka'
            ]);

            Agent::create([
                'employee_number' => '10072032',
                'name' => 'Dela Paz, Eldon'
            ]);

            Agent::create([
                'employee_number' => '10072023',
                'name' => 'De Jesus, Bryan Michael'
            ]);

            Agent::create([
                'employee_number' => '10072007',
                'name' => 'Tomo, Irand Alyssa'
            ]);

            Agent::create([
                'employee_number' => '10072030',
                'name' => 'Oqueno, Ana Coleen'
            ]);
            
            Agent::create([
                'employee_number' => '10072025',
                'name' => 'De Guzman, Rosette'
            ]);

            Agent::create([
                'employee_number' => '10072024',
                'name' => 'Malig, Raymond'
            ]);

            Agent::create([
                'employee_number' => '10072013',
                'name' => 'Cacabelos, Jumar'
            ]);

            Agent::create([
                'employee_number' => '10072026',
                'name' => 'Lat, Dianne Grace'
            ]);

            Agent::create([
                'employee_number' => '10072008',
                'name' => 'Valenzuela, Janis'
            ]);
        });
    }
}
