<?php

use Illuminate\Database\Seeder;

use App\{ Role, User};

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function(){
            $ryan = User::create([
                'name' => 'Ryan Pasquin',
                'email' => 'ryan.pasquin@personiv.com',
                'password' => bcrypt('!welcome17')
            ]);

            $ryan->roles()->attach([
                Role::where('name', 'manage-settings')->first()->id,
                Role::where('name', 'compose-questionnaires')->first()->id,
                Role::where('name', 'view-all-reports')->first()->id,
                Role::where('name', 'manage-reports')->first()->id,
                Role::where('name', 'delete-reports')->first()->id,
            ]);
            
            $glenn = User::create([
                'name' => 'Glenn Guido',
                'email' => 'glenn.guido@personiv.com',
                'password' => bcrypt('!welcome17')
                ]);

            $glenn->roles()->attach([
                Role::where('name', 'view-all-reports')->first()->id,
                Role::where('name', 'manage-reports')->first()->id,
                Role::where('name', 'delete-reports')->first()->id,
            ]);
        });
    }
}
