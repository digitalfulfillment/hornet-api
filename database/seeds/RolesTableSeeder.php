<?php

use Illuminate\Database\Seeder;

use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function(){
            Role::create([
                'name' => 'manage-settings',
                'description' => 'manage settings',
            ]);
            
            Role::create([
                'name' => 'compose-questionnaires',
                'description' => 'manage questionnaires',
            ]);
            
            Role::create([
                'name' => 'view-all-reports',
                'description' => 'view all reports',
            ]);

            Role::create([
                'name' => 'manage-reports',
                'description' => 'create and update reports',
            ]);

            Role::create([
                'name' => 'delete-reports',
                'description' => 'delete reports',
            ]);

            Role::create([
                'name' => 'manage-users',
                'admin_action' => true,
                'description' => 'manage users',
            ]);
                
            Role::create([
                'name' => 'developer',
                'admin_action' => true,
                'description' => 'developer',
            ]);
        });
    }
}
