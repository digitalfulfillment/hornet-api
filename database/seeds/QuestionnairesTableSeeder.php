<?php

use Illuminate\Database\Seeder;
use App\Questionnaire;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function() {
            Questionnaire::create([
                'name' => 'Call Assessment Form',
                'content' => [
                    [
                        'name' => 'CALL OPENING ‑ INTRODUCTION/GREETING',
                        'questions' => [
                            [
                                'question' => 'Did the agent greet the client and introduce themselves?',
                                'points' => 6
                            ],
                            [
                                'question' => 'Was the client advised that the call was being recorded when applicable?',
                                'points' => 6
                            ],
                        ]
                    ],
                    [
                        'name' => 'VERIFICATION OF POC/DECISION MAKER /DECISION MAKER',
                        'questions' => [
                            [
                                'question' => 'Did the agent confirm they were speaking with the POC/Decision Maker before discussing the account?',
                                'points' => 6
                            ],
                        ]
                    ],
                    [
                        'name' => 'CLIENT EXPERIENCE',
                        'questions' => [
                            [
                                'question' => 'Did the agent identify the reason for the call?',
                                'points' => 6
                            ],
                            [
                                'question' => 'Was the agent able to articulate and clearly communicate with the client in a way they  could understand?',
                                'points' => 6
                            ],
                            [
                                'question' => 'Did the agent address the client by name during the call?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent use please and thank you when communicating with the client?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent use appropriate pitch, tone, inflection and pace to sound helpful, energetic and friendly throughout the call?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent ensure there were no interruptions while the client was speaking?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent manage the call effectively, minimizing any dead air?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent express empathy when needed or warranted?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent display ownership and make an effort to troubleshoot the issue, if applicable?',
                                'points' => 6
                            ],
                            [
                                'question' => 'Did the agent resolve the issue, or provide the case information to the client, including date/time CM will follow up?',
                                'points' => 6
                            ],
                        ]
                    ],
                    [
                        'name' => 'PROCESS AND PROCEDURES',
                        'questions' => [
                            [
                                'question' => 'Did the agent exhibit a good working knowledge of thryv products and software  configuration?',
                                'points' => 6,
                            ],
                            [
                                'question' => 'Was screen sharing offered during the call?',
                                'points' => 4,
                            ],
                            [
                                'question' => 'Did the agent create the appropriate case in Salesforce?',
                                'points' => 6,
                            ],
                            [
                                'question' => 'Did the agent disposition the call correctly in Five9?',
                                'points' => 6,
                            ],
                        ]
                    ],
                    [
                        'name' => 'CALL CLOSING',
                        'questions' => [
                            [
                                'question' => 'Did the agent recap the call and state any action items, if applicable?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent ask if the client had any additional questions before the call ended?',
                                'points' => 4
                            ],
                            [
                                'question' => 'Did the agent thank or appreciate the client for their business in some way?',
                                'points' => 4
                            ],
                        ]
                    ],
                ]
            ]);

            Questionnaire::create([
                'name' => 'Chat Assessment Form',
                'content' => [
                    [
                        'name' => 'CHAT OPENING',
                        'questions' => [
                            [
                                'question' => 'Did the agent ask for the name, business name, and number associated with the account?',
                                'points' => 7
                            ],
                        ]
                    ],
                    [
                        'name' => 'CLIENT EXPERIENCE',
                        'questions' => [
                            [
                                'question' => 'Did the agent restate the reason for the chat?',
                                'points' => 6
                            ],
                            [
                                'question' => 'Was the agent able to clearly communicate with the client in a way they could understand?',
                                'points' => 10
                            ],
                            [
                                'question' => 'Did the agent address the client by name during the chat?',
                                'points' => 7
                            ],
                            [
                                'question' => 'Did the agent use please and thank you when communicating with the client?',
                                'points' => 10
                            ],
                        ]
                    ],
                    [
                        'name' => 'PROCESS AND PROCEDURES',
                        'questions' => [
                            [
                                'question' => 'Did the agent exhibit a good working knowledge of thryv products and software  configuration?',
                                'points' => 9
                            ],
                            [
                                'question' => 'Did the agent resolve the issue, or advise that a case was opened and inform them the CM will contact them within 24‑ 48 hours?',
                                'points' => 9
                            ],
                            [
                                'question' => 'Did the agent accurately edit the appropriate case in Salesforce?',
                                'points' => 8
                            ],
                        ]
                    ],
                    [
                        'name' => 'CHAT CLOSING',
                        'questions' => [
                            [
                                'question' => 'Did the agent recap the chat and state any action items, if applicable?',
                                'points' => 8
                            ],
                            [
                                'question' => 'Did the agent ask if the client had any additional questions before the chat ended?',
                                'points' => 8
                            ],
                            [
                                'question' => 'Did the agent thank or appreciate the client for their business in some way?',
                                'points' => 8
                            ],
                        ]
                    ],
                ]
            ]);
        });
    }
}
