<?php

use Illuminate\Database\Seeder;
use App\WorkQueue;

class WorkQueuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkQueue::create([
            'name' => 'Voice',
        ]);
        
        WorkQueue::create([
            'name' => 'Chat',
        ]);
    }
}
