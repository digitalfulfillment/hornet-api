<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->unsigned();
            $table->integer('campaign_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('work_queue_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('reference_code');
            $table->dateTime('call_history');
            $table->text('call_details')->nullable();
            $table->text('content');
            $table->boolean('auto_fail');
            $table->float('score');
            $table->float('total_points');
            $table->float('total_items');
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
