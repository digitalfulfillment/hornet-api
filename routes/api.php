<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'agent'], function () {
    Route::post('check-duplicate', 'AgentController@checkDuplicate');
    Route::post('search', 'AgentController@search');
});

Route::group(['prefix' => 'campaign'], function () {
    Route::post('check-duplicate', 'CampaignController@checkDuplicate');
    Route::post('search', 'CampaignController@search');
});

Route::group(['prefix' => 'evaluation'], function () {
    Route::post('check-duplicate', 'EvaluationController@checkDuplicate');
    Route::post('search', 'EvaluationController@search');
});

Route::group(['prefix' => 'team'], function () {
    Route::post('check-duplicate', 'TeamController@checkDuplicate');
    Route::post('search', 'TeamController@search');
});

Route::group(['prefix' => 'work-queue'], function () {
    Route::post('check-duplicate', 'WorkQueueController@checkDuplicate');
    Route::post('search', 'WorkQueueController@search');
});

Route::group(['prefix' => 'questionnaire'], function () {
    Route::post('check-duplicate', 'QuestionnaireController@checkDuplicate');
    Route::post('search', 'QuestionnaireController@search');
});

Route::group(['prefix' => 'user'], function () {
    Route::post('check', 'UserController@check');
    Route::post('check-duplicate', 'UserController@checkDuplicate');
    Route::post('check-password', 'UserController@checkPassword');
    Route::post('change-password', 'UserController@changePassword');
    Route::post('search', 'UserController@search');
});

Route::group(['prefix' => 'dashboard'], function() {
    Route::post('', 'DashboardController@index');
    Route::post('refresh', 'DashboardController@refresh');
    Route::post('work-queue', 'DashboardWorkQueueController@index');
});

Route::group(['prefix' => 'exception-report'], function() {
    Route::post('', 'ExceptionReportController@index');
    Route::post('test', 'ExceptionReportController@test');
});

Route::resource('agent', 'AgentController');
Route::resource('campaign', 'CampaignController');
Route::resource('evaluation', 'EvaluationController');
Route::resource('questionnaire', 'QuestionnaireController');
Route::resource('team', 'TeamController');
Route::resource('work-queue', 'WorkQueueController');
Route::resource('user', 'UserController');
Route::resource('role', 'RoleController');