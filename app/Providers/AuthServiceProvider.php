<?php

namespace App\Providers;

use App\{ Agent, Campaign, Evaluation, Questionnaire, Team, User, WorkQueue };
use App\Policies\{ AgentPolicy, CampaignPolicy, EvaluationPolicy, QuestionnairePolicy, TeamPolicy, UserPolicy, WorkQueuePolicy };

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Agent' => 'App\Policies\AgentPolicy',
        'App\Campaign' => 'App\Policies\CampaignPolicy',
        'App\Evaluation' => 'App\Policies\EvaluationPolicy',
        'App\Questionnaire' => 'App\Policies\QuestionnairePolicy',
        'App\Team' => 'App\Policies\TeamPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\WorkQueue' => 'App\Policies\WorkQueuePolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
