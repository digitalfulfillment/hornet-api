<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Evaluation;
use App\Traits\Evaluation\{ Overall, ScoreCard };

class EvaluationDownloadController extends Controller
{
    use Overall, ScoreCard;

    public function overall($from, $to)
    {
        $this->from = Carbon::parse("$from 00:00:00");
        $this->to = Carbon::parse("$to 23:59:59");

        return $this->fetchEvaluationsForDownload()
            ->prepareEvaluationsData()
            ->setQuestionsReference()
            ->getEvaluationResponses()
            ->downloadAsExcel($this->format);
    }

    public function scoreCard(Evaluation $evaluation)
    {
        return $this->prepareScoreCard($evaluation)->downloadScoreCard();
    }
}
