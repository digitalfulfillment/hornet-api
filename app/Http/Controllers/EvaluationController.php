<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use Carbon\Carbon;
use Excel;
use DB;
use App\Http\Requests\{ DuplicateEvaluation, StoreEvaluation, UpdateEvaluation };
use App\Events\{ EvaluationCreated, EvaluationUpdated, EvaluationDeleted };

class EvaluationController extends Controller
{
    protected $dataPerRequest = 10;

    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a paginated listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Evaluation::withTrashedRelatedModels()
            ->with('user')
            ->orderBy('created_at', 'desc')
            ->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', 'App\Evaluation');

        return response()->json(true);
    }

    /**
     * Check for existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkDuplicate(DuplicateEvaluation $request)
    {
        return response()->json(Evaluation::checkDuplicate() ? true : false);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEvaluation $request)
    {
        if(Evaluation::checkDuplicate()) abort(422, 'Reference ID already has an evaluation.');

        DB::transaction(function() use($request) {
            Evaluation::prepare($request->all())
                ->setCallHistory($request->call_history)
                ->setScore($request->content)
                ->setUser($request->user())
                ->save();
    
            event(new EvaluationCreated($request->user()));
        });

        return response()->json(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(Evaluation $evaluation)
    {
        $this->authorize('view', $evaluation);

        $evaluation->load('user')->loadTrashedRelatedModels();

        return $evaluation;
    }

    /**
     * Search the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return Evaluation::search()->with('user')->orderBy('created_at', 'desc')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(Evaluation $evaluation)
    {
        $this->authorize('update', $evaluation);

        return response()->json(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEvaluation $request, Evaluation $evaluation)
    {
        if(Evaluation::checkDuplicate()) abort(422, 'Reference ID already has an evaluation.');
        
        DB::transaction(function() use($request, $evaluation) {
            $evaluation->renew()
                ->setCallHistory($request->call_history)
                ->setScore($request->content)
                ->setUser($request->user())
                ->save();
    
            event(new EvaluationUpdated($request->user(), $evaluation));
        });

        return response()->json(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Evaluation $evaluation)
    {
        $this->authorize('delete', $evaluation);

        DB::transaction(function() use($evaluation) {
            $month = Carbon::parse($evaluation->created_at)->format('F');
            $year = Carbon::parse($evaluation->created_at)->year;
            
            $evaluation->delete();

            event(new EvaluationDeleted($month, $year));
        });

        return response()->json(true);
    }
}
