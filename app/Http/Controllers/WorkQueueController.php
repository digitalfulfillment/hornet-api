<?php

namespace App\Http\Controllers;

use App\WorkQueue;
use App\Http\Requests\{ DuplicateWorkQueue, StoreWorkQueue, UpdateWorkQueue };
use Illuminate\Http\Request;

class WorkQueueController extends Controller
{
    protected $dataPerRequest = 10;
    
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return WorkQueue::orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Check for existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function checkDuplicate(DuplicateWorkQueue $request)
     {
         return response()->json(WorkQueue::checkDuplicate() ? true : false);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorkQueue $request)
    {
        return WorkQueue::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkQueue  $workQueue
     * @return \Illuminate\Http\Response
     */
    public function show(WorkQueue $workQueue)
    {
        return $workQueue;
    }

    /**
    * Search the specified resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function search()
    {
        return WorkQueue::search()->orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkQueue  $workQueue
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkQueue $workQueue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkQueue  $workQueue
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkQueue $request, WorkQueue $workQueue)
    {
        if(WorkQueue::checkDuplicate()) abort(422, 'The name has already been taken.'); 
        
        return WorkQueue::where('id', $workQueue->id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkQueue  $workQueue
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkQueue $workQueue)
    {
        $this->authorize('delete', $workQueue);
        
        return response()->json($workQueue->delete());
    }
}
