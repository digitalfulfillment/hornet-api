<?php

namespace App\Http\Controllers;

use App\Questionnaire;
use App\Http\Requests\{ DuplicateQuestionnaire, StoreQuestionnaire, UpdateQuestionnaire };
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    protected $dataPerRequest = 10;

    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Questionnaire::orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Check for existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkDuplicate(DuplicateQuestionnaire $request)
    {
        return response()->json(Questionnaire::checkDuplicate() ? true : false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreQuestionnaire $request)
    {
        return Questionnaire::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        return $questionnaire;
    }

    /**
     * Search the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return Questionnaire::search()->orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionnaire $questionnaire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionnaire $request, Questionnaire $questionnaire)
    {
        if(Questionnaire::checkDuplicate()) abort(422, 'The name has already been taken.'); 

        return response()->json($questionnaire->renew()->save());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Questionnaire  $questionnaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        $this->authorize('delete', $questionnaire);
        
        return response()->json($questionnaire->delete());
    }
}
