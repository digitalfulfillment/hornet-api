<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Http\Requests\{ DuplicateCampaign, StoreCampaign, UpdateCampaign };
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    protected $dataPerRequest = 10;
    
    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Campaign::orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Check for existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkDuplicate(DuplicateCampaign $request)
    {
        return response()->json(Campaign::checkDuplicate() ? true : false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCampaign $request)
    {
        return Campaign::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        return $campaign;
    }

    /**
     * Search the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return Campaign::search()->orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCampaign $request, Campaign $campaign)
    {
        if(Campaign::checkDuplicate()) abort(422, 'The name has already been taken.'); 
        
        return Campaign::where('id', $campaign->id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        $this->authorize('delete', $campaign);
        
        return response()->json($campaign->delete());
    }
}
