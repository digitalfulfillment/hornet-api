<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Http\Requests\{ DuplicateAgent, StoreAgent, UpdateAgent };
use Illuminate\Http\Request;

class AgentController extends Controller
{
    protected $dataPerRequest = 10;

    public function __construct() {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Agent::orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Check for existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkDuplicate(DuplicateAgent $request)
    {
        return response()->json(Agent::checkDuplicate() ? true : false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgent $request)
    {   
        return Agent::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        return $agent;
    }

    /**
     * Search the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return Agent::search()->orderBy('name')->paginate($this->dataPerRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgent $request, Agent $agent)
    {
        if(Agent::checkDuplicate()) abort(422, 'The employee number has already been taken.'); 
        
        return Agent::where('id', $agent->id)->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        $this->authorize('delete', $agent);

        return response()->json($agent->delete());
    }
}
