<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\{ ChangePassword, DuplicateUser, StoreUser, UpdateUser };
use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth:api');
    }

    public function check() 
    {
        return request()->user()->load('roles');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword(ChangePassword $request)
    {
        $request->user()->password = bcrypt($request->password);

        $request->user()->save();

        return response()->json(true);
    }

    /**
     * Check if password matches authenticated user's password.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkPassword(Request $request) 
    {
        return response()->json($request->user()->checkPassword($request->password));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::with('roles')->where('is_admin', false)->whereNotIn('id', [request()->user()->id])->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Check for existing resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function checkDuplicate(DuplicateUser $request)
     {
         return response()->json(User::checkDuplicate() ? true : false);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        DB::transaction(function($query) use($request) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
            
            $user->roles()->attach($request->roles);
        });

        return response()->json(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $this->authorize('view', $user);

        return $user->load('roles');
    }

    public function search(Request $request)
    {
        return User::search()->with('roles')->where('is_admin', false)->whereNotIn('id', [$request->user()->id])->paginate(10);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, User $user)
    {
        if(User::checkDuplicate()) abort(422, 'The email has already been taken.');

        DB::transaction(function($query) use($request, $user) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
            $user->roles()->sync($request->roles);
        });

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();

        return response()->json(true);
    }
}
