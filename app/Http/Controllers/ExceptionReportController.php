<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Notification;

use App\User;
use App\Http\Requests\ExceptionReport;
use App\Notifications\ErrorReport;

class ExceptionReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
         Notification::send($this->developers(), new ErrorReport($request->all(), $request->user()));
    }

    protected function developers()
    {
        return User::whereHas('roles', function($query) {
            $query->where('name', 'developer');
        })->get();
    }

    public function test()
    {
        abort(500, 'This is an error.');
    }
}
