<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repositories\DashboardRepository;

class DashboardController extends Controller
{
    protected $dashboard;

    public function __construct(DashboardRepository $dashboard)
    {
        $this->middleware('auth:api');
        $this->dashboard = $dashboard;
    }

    public function index(Request $request)
    {
        return $this->dashboard->get($request->month, $request->year);
    }

    public function refresh(Request $request)
    {
        return $this->dashboard->refresh($request->month, $request->year);
    }
    
}
