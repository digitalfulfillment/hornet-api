<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\{ Agent, Campaign, WorkQueue, Team };
use App\Traits\WorkingDaysInAMonth;

class DashboardWorkQueueController extends Controller
{
    use WorkingDaysInAMonth;

    protected $campaign, $work_queue, $teams;

    public function index(Request $request)
    {
        return $this->getCampaign($request->campaign_id)
            ->getWorkQueue($request->work_queue_id)
            ->setDates($request->month, $request->year)
            ->getTeams()
            ->getAgents()
            ->evaluationsPerAgent()
            ->agentsWeeklyAverageScores()
            ->agentsOverallWeeklyAverage()
            ->overAllAverage()
            ->prepareData();
    }

    protected function getCampaign($id)
    {
        $this->campaign = Campaign::find($id);

        return $this;
    }

    protected function getWorkQueue($id)
    {
        $this->work_queue = WorkQueue::find($id);
        
        return $this;
    }

    protected function getTeams()
    {
        $this->teams = Team::withTrashed()->with(['evaluations' => function($query) {
            $query->with(['agent' => function($query) { 
                $query ->withTrashed(); 
            }]);
            $query->with(['campaign' => function($query) { 
                $query ->withTrashed(); 
            }]);
            $query->with(['work_queue' => function($query) { 
                $query ->withTrashed(); 
            }]);
            $query->with(['team' => function($query) { 
                $query ->withTrashed(); 
            }]);
            $query->with('user');
            $query->where('campaign_id', $this->campaign->id);
            $query->where('work_queue_id', $this->work_queue->id);
            $query->whereBetween('call_history', [$this->from, $this->to]);
        }])
        ->whereHas('evaluations', function($query) {
            $query->where('campaign_id', $this->campaign->id);
            $query->where('work_queue_id', $this->work_queue->id);
            $query->whereBetween('call_history', [$this->from, $this->to]);
        })->get();

        return $this;
    }

    protected function getAgents()
    {
        $this->teams->each(function($team) {
            $team->agents = Agent::withTrashed()->whereHas('evaluations', function($query) {
                $query->where('campaign_id', $this->campaign->id);
                $query->where('work_queue_id', $this->work_queue->id);
                $query->whereBetween('call_history', [$this->from, $this->to]);
            })->get();
        });

        return $this;
    }

    protected function evaluationsPerAgent()
    {
        $this->teams->each(function($team) {
            $team->agents->each(function($agent) use($team) {
                $agent->evaluations = $team->evaluations->where('agent_id', $agent->id);
            });
        });

        return $this;
    }

    protected function agentsWeeklyAverageScores()
    {
        $this->teams->each(function($team) {
            $team->agents->each(function($agent) use($team) {
                $agent->scores = collect([]);
                
                $this->dateRanges->each(function($dateRange) use($agent){
                    $agent->scores->push(
                        round($agent->evaluations
                            ->where('call_history', '>=', Carbon::parse($dateRange['from']))
                            ->where('call_history', '<=', Carbon::parse($dateRange['to']))
                            ->avg('score')
                        , 2)
                    );
                });
            });
        });

        return $this;
    }

    protected function agentsOverallWeeklyAverage()
    {
        $this->teams->each(function($team) {
            $team->agents->each(function($agent) use($team) {
                $agent->average = round($agent->evaluations->avg('score'), 2);
            });
        });

        return $this;
    }

    protected function overAllAverage()
    {
        $this->teams->each(function($team) {
            $team->average = round($team->evaluations->avg('score'), 2);
        });

        return $this;
    }

    protected function prepareData()
    {
        return collect([
            'campaign' => $this->campaign,
            'work_queue' => $this->work_queue,
            'teams' => $this->teams,
            'dateRanges' => $this->dateRanges,
        ]);
    }
}
