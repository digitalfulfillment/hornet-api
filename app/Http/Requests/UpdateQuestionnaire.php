<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Questionnaire;

class UpdateQuestionnaire extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $questionnaire = Questionnaire::find(request()->id);

        return $questionnaire && $this->user()->can('update', $questionnaire);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'content' => 'required|array',
            'content.*.name' => 'required|string',
            'content.*.questions' => 'required|array',
            'content.*.questions.*.question' => 'required|string',
            'content.*.questions.*.points' => 'required|numeric',
        ];
    }
}
