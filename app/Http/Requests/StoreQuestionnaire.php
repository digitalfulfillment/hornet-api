<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Questionnaire;

class StoreQuestionnaire extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Questionnaire::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:questionnaires',
            'content' => 'required|array',
            'content.*.name' => 'required|string',
            'content.*.questions' => 'required|array',
            'content.*.questions.*.question' => 'required|string',
            'content.*.questions.*.points' => 'required|numeric',
        ];
    }
}
