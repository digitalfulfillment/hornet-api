<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\WorkQueue;

class UpdateWorkQueue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $workQueue = WorkQueue::find(request()->id);

        return $workQueue && $this->user()->can('update', $workQueue);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
        ];
    }
}
