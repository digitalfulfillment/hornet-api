<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Evaluation;

class UpdateEvaluation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $evaluation = Evaluation::find(request()->id);

        return $evaluation && $this->user()->can('update', $evaluation);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agent_id' => 'required|numeric',
            'call_details' => 'array',
            'call_details.*.label' => 'required|string', 
            'call_details.*.value' => 'required|string',
            'call_history' => 'required|string',
            'campaign_id' => 'required|numeric',
            'content' => 'required|array',
            'content.*.name' => 'required|string',
            'content.*.questions' => 'required|array',
            'content.*.questions.*.question' => 'required|string',
            'content.*.questions.*.applicable' => 'required|boolean',
            'content.*.questions.*.points' => 'required|numeric',
            'content.*.questions.*.score' => 'required|numeric',
            'reference_code' => 'required|string',
            'auto_fail' => 'required|boolean',
        ];
    }
}
