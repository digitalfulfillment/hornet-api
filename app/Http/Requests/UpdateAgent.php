<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Agent;

class UpdateAgent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $agent = Agent::find(request()->id);

        return $agent && $this->user()->can('update', $agent);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_number' => 'required|string',
            'name' => 'required|string',
        ];
    }
}
