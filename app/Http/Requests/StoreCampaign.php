<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Campaign;

class StoreCampaign extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
         return $this->user()->can('create', Campaign::class);
     }
 
     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
     public function rules()
     {
         return [
             'name' => 'required|string|unique:campaigns',
         ];
     }
}
