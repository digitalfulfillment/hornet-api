<?php

namespace App\Http\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\{ Campaign, Evaluation, WorkQueue};
use App\Traits\WorkingDaysInAMonth;
use Carbon\Carbon;

class DashboardRepository
{
    use WorkingDaysInAMonth;
    
    protected $campaigns;

    public function get($month, $year)
    {
        return Cache::remember('dashboard_'. $month . '_' . $year, 60, function () use($month, $year) {
            return $this->setDates($month, $year)
                ->getCampaigns()
                ->getWorkQueues()
                ->getEvaluations()
                ->workQueueWeeklyAverage()
                ->workQueuesWeeklyOverall()
                ->workQueueMonthlyAverage()
                ->workQueuesMonthlyOverall()
                ->prepareResponse();
        });
    }

    public function refresh($month, $year)
    {
        Cache::forget('dashboard_'. $month . '_' . $year);

        return $this->get($month, $year);
    }

    protected function getCampaigns()
    {
        $this->campaigns = Campaign::with(['evaluations' => function($query) {
                $query->with('work_queue');
                $query->whereBetween('call_history', [$this->from, $this->to]);
            }])->get();

        return $this;
    }

    protected function getWorkQueues()
    {
        $this->campaigns->each(function($campaign) {
            $campaign->work_queues = WorkQueue::whereHas('evaluations', function($query) use($campaign){
                    $query->where('campaign_id', $campaign->id);
                    $query->whereBetween('call_history', [$this->from, $this->to]);
                })->get();
        });

        return $this;
    }

    protected function getEvaluations()
    {
        $this->campaigns->each(function($campaign) {
    
            $campaign->work_queues->each(function($workQueue) use($campaign) {
                
                $workQueue->evaluations = $campaign->evaluations->where('work_queue_id', $workQueue->id);
            });
        });

        return $this;
    }

    protected function workQueueWeeklyAverage()
    {
        $this->campaigns->each(function($campaign) {
            $campaign->work_queues->each(function($workQueue) {
                $workQueue->scores = collect([]);
    
                $this->dateRanges->each(function($dateRange) use($workQueue) {
                    $workQueue->scores->push(
                        round($workQueue->evaluations
                            ->where('call_history', '>=', Carbon::parse($dateRange['from']))
                            ->where('call_history', '<=', Carbon::parse($dateRange['to']))
                            ->avg('score'), 2)
                    );

                    $workQueue->scores->each(function($score) {
                        $score = round($score, 2);
                    });
                });
            });
        });

        return $this;
    }

    protected function workQueuesWeeklyOverall()
    {
        $this->campaigns->each(function($campaign) {
            $campaign->weekly_overall = collect([]);
    
            $this->dateRanges->each(function($dateRange, $dateIndex) use($campaign) {
                $campaign->weekly_overall->push(
                    round($campaign->work_queues->flatMap(function($workQueue) use($dateRange){
                        return $workQueue->evaluations
                            ->where('call_history', '>=', Carbon::parse($dateRange['from']))
                            ->where('call_history', '<=', Carbon::parse($dateRange['to']));
                    })->avg('score'), 2)
                );
            });
        });

        return $this;
    }

    protected function workQueueMonthlyAverage()
    {
        $this->campaigns->each(function($campaign) {
            $campaign->work_queues->each(function($workQueue){
                $workQueue->average = round($workQueue->evaluations->avg('score'), 2);
            });
        });

        return $this;
    }

    protected function workQueuesMonthlyOverall()
    {
        $this->campaigns->each(function($campaign) {
            $campaign->average = round($campaign->evaluations->avg('score'), 2);
        });

        return $this;
    }

    protected function prepareResponse()
    {
        return collect([
            'dateRanges' => $this->dateRanges,
            'campaigns' => $this->campaigns
        ]);
    }
}
