<?php

namespace App\Traits;

trait Preparable
{
    /**
     * Prepare an instance with the request payload.
     */
    public static function prepare() 
    {
        $classInstance = new static;

        foreach ($classInstance->fillable as $column) {
            $classInstance->$column = request()->$column;
        }

        return $classInstance;
    }

    /**
     * Renew an instance with the request payload.
     */
    public function renew()
    {
        foreach ($this->fillable as $column) {
            $this->$column = request()->$column;
        }

        return $this;
    }
}
