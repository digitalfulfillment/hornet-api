<?php

namespace App\Traits\Evaluation;

use App\Evaluation;
use Carbon\Carbon;
use Excel;

trait Overall
{
    protected $evaluations, $from, $questions, $to;
    
    protected $format = 'xlsx';

    protected function fetchEvaluationsForDownload() 
    {
        $this->evaluations = Evaluation::withTrashedRelatedModels()
            ->with('user')
            ->whereBetween('call_history', [$this->from, $this->to])
            ->get();
        
        return $this;
    }

    protected function prepareEvaluationsData()
    {
        $this->evaluations->each(function($evaluation) {
            $evaluation->call_details = collect($evaluation->call_details)->map(function($callDetail){
                return $callDetail['label'] .': '. $callDetail['value'];
            })->flatten();

            $evaluation->questions = collect([]);
        });

        return $this;
    }

    protected function setQuestionsReference()
    {
        $this->questions = $this->evaluations->flatMap(function($evaluation){
                return $evaluation->content;
            })
            ->flatMap(function($category) {
                return $category['questions'];
            })
            ->unique('question');
        
        return $this;
    }

    protected function getEvaluationResponses() 
    {
        $this->questions->each(function($question) {
            $this->evaluations->each(function($evaluation) use($question) {
                $responses = collect($evaluation->content)->flatMap(function($category) {
                    return $category['questions'];
                });

                $evalQuestion = $responses->where('question', $question['question'])->first();
                
                if(!$evalQuestion) $evaluation->questions->push(null);
                
                else {
                    if(!$evalQuestion['applicable']) {
                        $evaluation->questions->push([
                            'rating' => 'N/A',
                            'remarks' => $evalQuestion['remarks'],
                            ]);
                        }
                        
                    else if($evalQuestion['applicable'] && $evalQuestion['score']) {
                        $evaluation->questions->push([
                            'rating' => 'Yes',
                            'remarks' => $evalQuestion['remarks'],
                            ]);
                        } 
                        
                    else if($evalQuestion['applicable'] && !$evalQuestion['score']) {
                        $evaluation->questions->push([
                            'rating' => 'No',
                            'remarks' => $evalQuestion['remarks'],
                        ]);
                    }
                } 
            });
        });

        return $this;
    }

    protected function downloadAsExcel()
    {
        return Excel::create('Data Extraction from ' . $this->from->toFormattedDateString() . ' to ' . $this->to->toFormattedDateString(), 
            function($excel) {

                $excel->sheet('Evaluations', function($sheet) {

                    $sheet->loadView('excel.evaluations')->with('evaluations', $this->evaluations)->with('questions', $this->questions);
                });
                
                $excel->sheet('Questions', function($sheet) {

                    $sheet->loadView('excel.questions')->with('questions', $this->questions);
                });
            })
        ->download($this->format);
    }
}
