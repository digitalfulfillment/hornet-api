<?php

namespace App\Traits\Evaluation;

use Excel;

trait ScoreCard
{
    protected $evaluation;

    protected function prepareScoreCard($evaluation) 
    {
        $this->evaluation = $evaluation->load('user')
            ->load(['agent' => function($query) {
                $query->withTrashed();
            }])
            ->load(['campaign' => function($query) {
                $query->withTrashed();
            }])
            ->load(['team' => function($query) {
                $query->withTrashed();
            }])
            ->load(['work_queue' => function($query) {
                $query->withTrashed();
            }]);

        return $this;
    }

    protected function downloadScoreCard()
    {
        return Excel::create($this->evaluation->work_queue->name . ' Assessment Form - #'. $this->evaluation->reference_code, 
            function($excel) {

                $excel->sheet('Sheet 1', function($sheet) {

                    $sheet->loadView('excel.score-card')->with('evaluation', $this->evaluation);
                });
                
            })
        ->download($this->format);
    }
}
