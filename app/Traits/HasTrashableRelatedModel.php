<?php

namespace App\Traits;

trait HasTrashableRelatedModel
{
    public static function withTrashedRelatedModels() 
    {
        $classInstance = new static;

        $model = get_called_class()::query();

        foreach ($classInstance->trashableRelatedModels as $relationship) {
            $model->with([$relationship => function($query) {
                $query->withTrashed();
            }]);
        }

        return $model;
    }

    public function loadTrashedRelatedModels()
    {
        foreach ($this->trashableRelatedModels as $relationship) {
            $this->load([$relationship => function($query) {
                $query->withTrashed();
            }]);
        }

        return $this;
    }
}
