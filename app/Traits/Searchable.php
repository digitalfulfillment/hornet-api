<?php

namespace App\Traits;

trait Searchable
{
    public static function search() {
        $classInstance = new static;
        
        $model = get_called_class()::query();

        $model->where(function($query) use($classInstance) {
            foreach ($classInstance->findBy as $column) {
                if(request()->has($column)) $query->orWhere($column, 'like', request()->$column.'%');
            }
        });


        return $model;
    }

    public static function searchByRelationship() {
        $classInstance = new static;
        
        $model = get_called_class()::query();

        foreach ($classInstance->findByRelationship as $relationship) {
            if(request()->has($relationship)) 
            {
                $model->orWhereHas($relationship, function($query) use($relationship) {
                    $query->where(request()->input("$relationship.column"), 'like', request()->input("$relationship.value").'%');
                });
            }
        }

        return $model;
    }
}
