<?php

namespace App\Traits;
use Illuminate\Support\Facades\Schema;

trait Unique {
    public static function checkDuplicate() 
    {
        $classInstance = new static;
        
        $model = get_called_class()::query();

        if (Schema::hasColumn($classInstance->getTable(), 'deleted_at')) {
            $model->withTrashed();
        }

        foreach($classInstance->uniqueColumns as $key => $column) {
            $model->where($column, request()->$column);
        }

        if(request()->has('id')) {
            $duplicate = $model->whereNotIn('id', [request()->id])->first();
        }
        else {
            $duplicate = $model->first();
        }

        return $duplicate;
    }
}