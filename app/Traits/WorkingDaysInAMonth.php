<?php

namespace App\Traits;

use Carbon\Carbon;

trait WorkingDaysInAMonth
{
    protected $dateRanges, $from, $to;

    protected function setDates($month, $year = 0)
    {
        if(!$year) $year = Carbon::now()->year;

        if(Carbon::parse("first day of $month $year")->dayOfWeek == Carbon::SATURDAY) $firstDay = Carbon::parse("first day of $month $year")->addDays(2);
        else if(Carbon::parse("first day of $month $year")->dayOfWeek == Carbon::SUNDAY) $firstDay = Carbon::parse("first day of $month $year")->addDay();
        else $firstDay = Carbon::parse("first day of $month $year");
        
        $lastDay = Carbon::parse("last day of $month $year");
        
        $this->from = Carbon::parse($firstDay);
        $this->to = Carbon::parse($lastDay)->setTime(23, 59, 59);

        $this->dateRanges = collect([]);

        for ($from = Carbon::parse($firstDay); $from->lte($lastDay); $from->addWeek()) { 
            if($from->eq($firstDay))
            {
                $this->dateRanges->push([
                    'from' => Carbon::parse($from),
                    'to' => Carbon::parse($from)->endOfWeek()->setTime(23, 59, 59),
                ]);
            }
            
            else if(Carbon::parse($from)->addWeek()->gte($lastDay))
            {   
                if(!$from->isLeapYear() && $from->month === 2 && $from->ne($lastDay))
                {
                    $from->subDay();
                    $this->dateRanges->push([
                        'from' => Carbon::parse($from)->startOfWeek(),
                        'to' => Carbon::parse($from)->endOfWeek()->setTime(23, 59, 59),
                    ]);
                } else {
                    $this->dateRanges->push([
                        'from' => Carbon::parse($from)->startOfWeek(),
                        'to' => Carbon::parse($lastDay)->setTime(23, 59, 59),
                    ]);
                }
            }

            else {
                $this->dateRanges->push([
                    'from' => Carbon::parse($from)->startOfWeek(),
                    'to' => Carbon::parse($from)->endOfWeek()->setTime(23, 59, 59),
                ]);
            }
        }

        return $this;
    }
}
