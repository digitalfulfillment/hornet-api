<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\{HasTrashableRelatedModel, Preparable, Unique};
use Carbon\Carbon;

class Evaluation extends Model
{
    use HasTrashableRelatedModel, Preparable, Unique;

    protected $dates = ['call_history', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
        'call_details' => 'array',
    ];
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'agent_id', 
        'team_id', 
        'campaign_id', 
        'work_queue_id', 
        'reference_code',
        'call_details',
        'auto_fail',
        'content',
        'remarks',
    ];

    /**
     * The attributes that are reference for searching.
     *
     * @var array
     */
    protected $findBy = ['reference_code'];

    /**
     * The date attributes that are reference for searching.
     *
     * @var array
     */
    protected $findByDates = ['call_history', 'created_at', 'updated_at'];

    /**
    * The relationship that are searchable.
    *
    * @var array
    */
    protected $findByRelationship = [
        'agent',
        'team',
        'campaign',
        'work_queue',
    ];

    /**
    * The relationships that implements soft deletes.
    *
    * @var array
    */
    protected $trashableRelatedModels = [
        'agent',
        'team',
        'campaign',
        'work_queue',
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->timezone('America/Chicago')->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->timezone('Asia/Singapore')->toDateTimeString();
    }

    /**
     * The attributes that must be unique.
     *
     * @var array
     */
    protected $uniqueColumns = ['reference_code', 'work_queue_id'];

    /**
     * Get the agent of the evaluation.
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    
    /**
     * Get the campaign of the evaluation.
     */
    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }
    
    /**
     * Get the team of the evaluation.
     */
    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    /**
     * Get the user of the evaluation.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Get the work queue of the evaluation.
     */
    public function work_queue()
    {
        return $this->belongsTo('App\WorkQueue');
    }
            
    /**
     * Set the call history of the evaluation.
     *
     * @param string $call_history
     * @return \App\Questionnaire
     */
    public function setCallHistory($call_history)
    {
        $this->call_history = Carbon::parse($call_history);

        return $this;
    }
        
    /**
     * Set the call history of the evaluation.
     *
     * @param array $content
     * @return \App\Questionnaire
    */
    public function setScore($content)
    {
        $content = collect($content);

        $this->total_points = $this->totalPoints($content);
        $this->total_items = $this->totalItems($content);
        $this->score = ($this->total_points / $this->total_items) * 100;

        return $this;
    }

    /**
     * Set the evaluator id of the evaluation.
     *
     * @param array $content
     * @return \App\Questionnaire
    */
    public function setUser(User $user) 
    {
        $this->user_id = $user->id;

        return $this;
    }

    /**
     * Maps question items if it is applicable.
     *
     * @param collection $questions
     * @return integer
    */
    private function subTotalItems($questions)
    {
        return $questions
            ->map(function($question){
                if($question['applicable']) return (int)$question['points'];
                return 0;
            })
            ->reduce(function($sum, $value) {
                return $sum + $value;
            });
    }

    /**
     * Maps question scores if it is applicable.
     *
     * @param collection $questions
     * @return integer
    */
    private function subTotalPoints($questions)
    {
        return $questions
            ->map(function($question){
                if($question['applicable'])  return (int)$question['score'];
                return 0;
            })
            ->reduce(function($sum, $value) {
                return $sum + $value;
            });
    }
    
    /**
     * Maps every content and sums all point.
     *
     * @param array $content
     * @return integer
    */
    private function totalItems($content) 
    {
        return $content
            ->map(function($item){
                return $this->subtotalItems(collect($item['questions']));
            })
            ->reduce(function($sum, $value) {
                return $sum + $value;
            });
    }

    /**
     * Maps every content and sums all scores.
     *
     * @param array $content
     * @return integer
    */
    private function totalPoints($content) 
    {
        return $content
            ->map(function($item){
                return $this->subtotalPoints(collect($item['questions']));
            })
            ->reduce(function($sum, $value) {
                return $sum + $value;
            });
    }

    public static function search() {
        $evaluation = Evaluation::query();
        
        $classInstance = new static;

        if(!request()->user()->is_admin && !request()->user()->hasRole(Role::where('name', 'view-all-reports')->first()))
        {
            $evaluation->where('user_id', request()->user()->id);
        }

        $evaluation->where(function($query) use($classInstance){
            foreach($classInstance->findByDates as $column) {
                if(request()->has($column)) $query->orWhereBetween($column, [
                    Carbon::parse(request()->input("$column.from")), 
                    Carbon::parse(request()->input("$column.to"). ' 23:59:59')
                ]);
            }
        });

        $evaluation->where(function($query) use($classInstance){
            foreach($classInstance->findByRelationship as $relationship) {
                if(request()->has($relationship)) {
                    $query->orWhereHas($relationship, function($query) use($relationship) {
                        $query->withTrashed()
                            ->where(
                                request()->input("$relationship.column"),
                                'like', 
                                request()->input("$relationship.value").'%'
                            );
                    });
                }
            }

            foreach($classInstance->findBy as $column) {
                if(request()->has($column)) $query->orWhere($column, 'like', request()->input($column).'%');
            }
        });
        
        foreach ($classInstance->trashableRelatedModels as $trashableRelatedModels) {
            $evaluation->with([$trashableRelatedModels => function($query) {
                $query->withTrashed();
            }]);
        }

        return $evaluation;
    }
}
