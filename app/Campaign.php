<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\{Searchable, Unique};

class Campaign extends Model
{
    use Searchable, SoftDeletes, Unique;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes that are reference for searching.
     *
     * @var array
     */
     protected $findBy = ['name'];
     
    /**
    * The attributes that must be unique.
    *
    * @var array
    */
    protected $uniqueColumns = ['name'];

    /**
     * Get the evaluations of the campaign.
     */
    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }
}
