<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\{Preparable, Searchable, Unique};

class Questionnaire extends Model
{
    use Preparable, Searchable, Unique;
    
    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
        'content' => 'array',
    ];
     
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name', 'content'];
    
    /**
    * The attributes that are reference for searching.
    *
    * @var array
    */
    protected $findBy = ['name'];
    
    /**
    * The attributes that must be unique.
    *
    * @var array
    */
    protected $uniqueColumns = ['name'];
}
