<?php

namespace App\Listeners;

use App\Events\EvaluationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

use App\Http\Repositories\DashboardRepository;

class EvaluationCreatedListener implements ShouldQueue
{
    protected $dashboard;
 
    /**
    * Create the event listener.
    *
    * @return void
    */
    public function __construct(DashboardRepository $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Handle the event.
     *
     * @param  EvaluationCreated  $event
     * @return void
     */
    public function handle(EvaluationCreated $event)
    {
        $month = Carbon::today()->format('F');
        
        $year = Carbon::now()->year;

        Cache::forget('dashboard_'. $month . '_' . $year);

        $this->dashboard->get($month, $year);
    }
}
