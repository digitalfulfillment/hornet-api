<?php

namespace App\Listeners;

use App\Events\EvaluationDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

use App\Http\Repositories\DashboardRepository;

class EvaluationDeletedListener implements ShouldQueue
{
    protected $dashboard;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DashboardRepository $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Handle the event.
     *
     * @param  EvaluationDeleted  $event
     * @return void
     */
    public function handle(EvaluationDeleted $event)
    {
        Cache::forget('dashboard_'. $event->month . '_' . $event->year);

        $this->dashboard->get($event->month, $event->year);
    }
}
