<?php

namespace App\Listeners;

use App\Events\EvaluationUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

use App\Http\Repositories\DashboardRepository;

class EvaluationUpdatedListener implements ShouldQueue
{
    protected $dashboard;
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DashboardRepository $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Handle the event.
     *
     * @param  EvaluationUpdated  $event
     * @return void
     */
    public function handle(EvaluationUpdated $event)
    {
        $month = Carbon::parse($event->evaluation->created_at)->format('F');

        $year = Carbon::parse($event->evaluation->created_at)->year();
     
        Cache::forget('dashboard_'. $month . '_' . $year);

        $this->dashboard->get($month, $year);
    }
}
