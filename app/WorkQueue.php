<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\{Searchable, Unique};

class WorkQueue extends Model
{
    use Searchable, SoftDeletes, Unique;

    /**
    * The attributes that should be casted to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['name'];
    
    /**
    * The attributes that are reference for searching.
    *
    * @var array
    */
    protected $findBy = ['name'];
   
    /**
    * The attributes that must be unique.
    *
    * @var array
    */
    protected $uniqueColumns = ['name'];

    /*
     * Get the evaluation record of work queue
     *
    */
    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }
}
