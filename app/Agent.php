<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\{Searchable, Unique};

class Agent extends Model
{
    use Searchable, SoftDeletes, Unique;

    /**
     * The attributes that should be casted to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['employee_number', 'name'];

    /**
     * The attributes that are reference for searching.
     *
     * @var array
     */
    protected $findBy = ['employee_number', 'name'];

    /**
     * The attributes that must be unique.
     *
     * @var array
     */
    protected $uniqueColumns = ['employee_number'];

    /**
     * Set the user's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(
            strtolower($value)
        );
    }

    /**
     * Get the evaluations of the agent.
     */
    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }
}
