<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes; 

use App\Traits\{ Searchable, Unique };

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Searchable, SoftDeletes, Unique;

    protected $uniqueColumns = ['email'];

    protected $findBy = ['name', 'email'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Set the user's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords(
            strtolower($value)
        );
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
    ];

    /**
     * Get the evaluations of the user.
     */
    public function evaluations()
    {
        return $this->hasMany('App\Evaluation');
    }

    /**
     * Get the roles of the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_roles');
    }

    /**
     * Checks the role of a user.
     *
     * @param string
     * @return boolean
     */
    public function hasRole(Role $role)
    {
        $this->load(['roles' => function($query) use($role) {
            $query->where('name', $role->name);
        }]);

        return count($this->roles);
    }

    /**
     * Checks the password of a the user.
     *
     * @param string
     * @return boolean
     */
    public function checkPassword($password)
    {
        return Hash::check($password, $this->password);
    }
}
