<?php

namespace App\Policies;

use App\{ Role, User };
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $authUser, User $user)
    {
        return $authUser->hasRole(
            Role::where('name', 'manage-users')->first()
        );
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(
            Role::where('name', 'manage-users')->first()
        );
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $authUser, User $user)
    {
        return $user && $authUser->hasRole(
            Role::where('name', 'manage-users')->first()
        );
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $authUser, User $user)
    {
        return $user && $authUser->hasRole(
            Role::where('name', 'manage-users')->first()
        );
    }
}
