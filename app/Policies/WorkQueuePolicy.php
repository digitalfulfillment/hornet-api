<?php

namespace App\Policies;

use App\User;
use App\WorkQueue;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorkQueuePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the workQueue.
     *
     * @param  \App\User  $user
     * @param  \App\WorkQueue  $workQueue
     * @return mixed
     */
    public function view(User $user, WorkQueue $workQueue)
    {
        return $user->hasRole(
            Role::where('name', 'manage-settings')->first()
        );
    }

    /**
     * Determine whether the user can create workQueues.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(
            Role::where('name', 'manage-settings')->first()
        );
    }

    /**
     * Determine whether the user can update the workQueue.
     *
     * @param  \App\User  $user
     * @param  \App\WorkQueue  $workQueue
     * @return mixed
     */
    public function update(User $user, WorkQueue $workQueue)
    {
        return $user->hasRole(
            Role::where('name', 'manage-settings')->first()
        );
    }

    /**
     * Determine whether the user can delete the workQueue.
     *
     * @param  \App\User  $user
     * @param  \App\WorkQueue  $workQueue
     * @return mixed
     */
    public function delete(User $user, WorkQueue $workQueue)
    {
        return $user->hasRole(
            Role::where('name', 'manage-settings')->first()
        );
    }
}
