<?php

namespace App\Policies;

use App\User;
use App\Evaluation;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class EvaluationPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the evaluation.
     *
     * @param  \App\User  $user
     * @param  \App\Evaluation  $evaluation
     * @return mixed
     */
    public function view(User $user, Evaluation $evaluation)
    {
        return $user->hasRole(Role::where('name', 'view-all-reports')->first()) || 
            ($evaluation->user_id == $user->id && $user->hasRole(Role::where('name', 'manage-reports')->first()));
    }

    /**
     * Determine whether the user can create evaluations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(
            Role::where('name', 'manage-reports')->first()
        );
    }

    /**
     * Determine whether the user can update the evaluation.
     *
     * @param  \App\User  $user
     * @param  \App\Evaluation  $evaluation
     * @return mixed
     */
    public function update(User $user, Evaluation $evaluation)
    {
        return
            $evaluation->user_id == $user->id && 
            $user->hasRole(
                Role::where('name', 'manage-reports')->first()
            );
    }

    /**
     * Determine whether the user can delete the evaluation.
     *
     * @param  \App\User  $user
     * @param  \App\Evaluation  $evaluation
     * @return mixed
     */
    public function delete(User $user, Evaluation $evaluation)
    {
        return  
            $user->hasRole(
                Role::where('name', 'delete-reports')->first()
            );
    }
}
