<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\User;

class ErrorReport extends Notification implements ShouldQueue
{
    use Queueable;

    protected $report, $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($report, User $user)
    {
        $this->report = $report;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello!')
                    ->line($this->user->name. " reported an error below.")
                    ->line($this->report['status']. '('.$this->report['statusText'].')')
                    ->line($this->report['message'])
                    ->action('Visit Application', 'http://localhost')
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'from' => $this->user,
            'message' => $this->report['message'],
            'status' => $this->report['status'],
            'statusText' => $this->report['statusText'],
        ];
    }
}
